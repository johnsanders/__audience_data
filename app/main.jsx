import React from "react";
import {render} from "react-dom";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import HomeContainer from "./components/HomeContainer";
import moment from "moment";
import "bootstrap/dist/css/bootstrap.css";
import "./styles/style.css";

render((
	<BrowserRouter basename="/apps/audience_data">
		<Switch>
			<Route
				path="*/"
				render={ props => <HomeContainer {...props} />}
			/>
		</Switch>
	</BrowserRouter>
), document.getElementById("app"));


