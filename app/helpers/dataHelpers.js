import axios from "axios";
import forOwn from "lodash/forOwn";
import sortBy from "lodash/sortBy";

const defaultFieldsFb = "id,uploadedTime,topics,";
const defaultFieldsFbRegions= "i.id,uploadedTime,region,country,total_time";
const fbUrl = "/apps/flashSQLinterface/read_audience_data.php?orderby=uploadedTime&order=ASC";
const fbTable = "talk_fb_insights";
const fbRegionsTable = "talk_fb_insights_regions";

const getStartDate = (startDate, showDate, isSingleShow) => (
	isSingleShow ? showDate.startOf("day").format() : startDate.format()
);
const getEndDate = (endDate, showDate, isSingleShow) => (
	isSingleShow ? showDate.endOf("day").format() : endDate.format()
);
const getRequestedItems = (selectedCategory, selectedItems) => (
	selectedCategory === "Viewer retention" ? ["viewsRetentionGraphs"] : selectedItems
);
const createCountryQuery = (selectedCategory, regionCountry) => {
	if (selectedCategory.includes("Countries")) {
		return "(country='" + regionCountry + "')";
	} else if (selectedCategory.includes("Regions")) {
		const arr = regionCountry.split("$");
		const region = arr[0];
		const country = arr[1];
		return "(region='" + region + "' AND country='" + country + "')";
	}
};

// WHEN WE QUERY FOR REGIONS, WE HAVE TO REFORMAT INTO WHAT THE CHART EXPECTS
const mergeRegionData = data => {
	return data.reduce( (accMerged, datum1) => {
		const dateAlreadyProcessed = accMerged.find( date => date.uploadedTime === datum1.uploadedTime);
		if (!dateAlreadyProcessed) {
			const regionsToday = data.filter( datum2 => datum2.uploadedTime === datum1.uploadedTime );
			const todayObject = regionsToday.reduce( (accToday, region) => {
				accToday[region.region + "$" + region.country] = region.total_time;	
				return accToday;
			}, {});
			todayObject.uploadedTime = regionsToday[0].uploadedTime;
			todayObject.topics = regionsToday[0].topics;
			accMerged.push(todayObject);
		}
		return accMerged;
	}, []);
};

// IF WE'RE DOING COUNTRIES, WE HAVE TO ADD UP ALL OF EACH COUNTRY'S REGIONS
const calcCountries = data => {
	const countryArrays = data.map( datum => {
		const countries = {uploadedTime:datum.uploadedTime, topics:datum.topics};
		forOwn(datum, (value, key) => {  
			if (key !== "uploadedTime" && key !== "topics") {
				const country = key.split("$")[1];
				if (countries.hasOwnProperty(country)){
					countries[country].push(value);
				} else {
					countries[country] = [value];
				}
			};
		});
		return countries;
	});
	const countryAverages = countryArrays.map( datum => {
		const ret = {uploadedTime:datum.uploadedTime, topics:datum.topics};
		forOwn(datum, (value, key) => {
			if (key !== "uploadedTime" && key !== "topics") {
				ret[key] = value.reduce( (acc, item) => acc + item, 0 );
			}
		});
		return ret;
	});
	return countryAverages;
};

export const calculateTopics = (data, selectedItem) => {
	const topicsStrings = data.reduce( (acc, datum) => {
		const showTopics = datum.topics.split(", ");
		showTopics.forEach( topic => (
			acc.includes(topic) ? null : acc.push(topic)		
		));
		return acc;
	}, []);
	const topics = topicsStrings.map( topicString => {
		const showsWithTopic = data.filter( datum => datum.topics.includes(topicString) );
		return {
			topic: topicString,
			[selectedItem]:Math.round(showsWithTopic.reduce( (acc, show) => acc + show[selectedItem], 0 ) / showsWithTopic.length)
		}
	});
	return sortBy(topics, selectedItem).reverse();
};

export const fetchFbData = (selectedCategory, selectedItems, startDate, endDate, showDate) => {
	const start = getStartDate(startDate, showDate, selectedCategory.includes("retention"));
	const end = getEndDate(endDate, showDate, selectedCategory.includes("retention"));
	const where = "&where=uploadedTime>='" + start + "' AND uploadedTime<='" + end + "'";
	const requestedItems = getRequestedItems(selectedCategory, selectedItems);
	const url = fbUrl + "&table=" + fbTable + where + "&fields=" + defaultFieldsFb + requestedItems.join(","); 
	return axios.get(url);
};

export const fetchFbRegionData = (selectedCategory, selectedItems, startDate, endDate) => {
	const regionQuery = selectedItems.reduce( (acc, regionCountry, i) => {
		let region, country;
		const orString = i < selectedItems.length-1 ? " OR " : "";
		const countryQuery = createCountryQuery(selectedCategory, regionCountry);
		return acc + countryQuery + orString;
	}, "" );
	const where = "&where=uploadedTime>='" + startDate.format() + "' " +
		"AND uploadedTime<='" + endDate.format() + "' " + 
		"AND (" + regionQuery + ")";
	const url = fbUrl + "&table=" + fbRegionsTable + where + "&fields=" + defaultFieldsFbRegions;
	console.log(url)
	return axios.get(url)
		.then( data => {
			const merged = mergeRegionData(data.data);
			return selectedCategory.includes("Countries") ? calcCountries(merged) : merged;
		});
};

