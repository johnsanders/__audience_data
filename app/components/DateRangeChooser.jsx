import React from "react";
import DatePicker from "react-datepicker";
import moment from "moment";
import 'react-datepicker/dist/react-datepicker.css';

const minDate = moment("2017-05-05");
console.log(minDate);
const maxDate = moment();
const labelStyle = {
	fontSize:"20px",
	textWeight:"bold",
	display:"block",
	textAlign:"center"
};
const pickerStyle = {
	margin:"0 5px"
};

const DateRangeChooser = props => (
	<div className="col-xs-6" style={{display:"flex"}}>
		<div style={pickerStyle}>
			<label
				htmlFor="startDate"
				style={labelStyle}
			>
				Start Date
			</label>
			<DatePicker
				id="startDate"
				className="form-control text-center"
				dayClassName={()=>"startDate"}
				dateFormat="D MMMM, YYYY"
				selected={props.startDate}
				selectsStart
				startDate={props.startDate}
				endDate={props.endDate}
				onChange={props.onDateChange}
				minDate={minDate}
				maxDate={maxDate}
			/>
		</div>
		<div style={pickerStyle}>
			<label
				htmlFor="startDate"
				style={labelStyle}
			>
				End Date
			</label>
			<DatePicker
				id="endDate"
				className="form-control text-center"
				dayClassName={()=>"endDate"}
				dateFormat="D MMMM, YYYY"
				selected={props.endDate}
				selectsEnd
				startDate={props.startDate}
				endDate={props.endDate}
				onChange={props.onDateChange}
				minDate={minDate}
				maxDate={maxDate}
			/>
		</div>
	</div>
);
export default DateRangeChooser;
