import React from "react";
import categories from "../config/categories";

const CategorySelector = props => {
	return (
		<div className="col-xs-6 text-center">
				<label
					htmlFor="categorySelect"
					style={{
						fontSize:"20px",
						textWeight:"bold",
						display:"block"
					}}
				>
					Data Category
				</label>
				<select 
					id="categorySelect"
					className="form-control"
					onChange={props.onCategoryChange}
				>
					{
						categories.map( category => (
							<option 
								key={"category_" + category.name}
								value={category.name}
							>
								{category.name}
							</option>
						))
					}
				</select>
		</div>
	)
};

export default CategorySelector;
