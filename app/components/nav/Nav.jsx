import React from "react";
import {Link} from "react-router-dom";
import logoImage from "../../img/cnni_logo.png";
import NavItem from "./NavItem";
import NavToolsMenu from "./NavToolsMenu";

class Nav extends React.Component {
	constructor(props) {
		super(props);
		this.state = {navForceVisibleClass:""};
		this.toggleNav = this.toggleNav.bind(this);
	}
	toggleNav() {
		if (this.state.navForceVisibleClass === "") {
			this.setState({navForceVisibleClass:"navForceVisible"});
		} else {
			this.setState({navForceVisibleClass:""});
		}
	}
	render() {
		return (
			<nav className="navbar navbar-default navbar-fixed-top">
				<div className="container-fluid">
					<div className="navbar-header">
						<a className="navbar-brand" style={{padding:0}} href="http://cnnitouch/">
							<img style={{height:"50px", width:"inherit"}} src={logoImage} />
						</a>
						<button
							type="button"
							id="navToggle"
							className="navbar-toggle collapsed"
							onClick={this.toggleNav}
						>
							<span className="icon-bar"></span>
							<span className="icon-bar"></span>
							<span className="icon-bar"></span>
						</button>
					</div>
					<div className={`collapse navbar-collapse ${this.state.navForceVisibleClass}`}>
						<ul className="nav navbar-nav">
							<NavItem
								iconClasses="glyphicon glyphicon-home"
								text="CNNI Touch"
								link="http://cnnitouch"
							/>
							<NavItem
								iconClasses="glyphicon glyphicon-upload"
								text="Image Uploader"
								link="/utilities/cnnimages_manual"
							/>
							<NavItem
								iconClasses="glyphicon glyphicon-picture"
								text="Getty Importer"
								link="/utilities/getty_importer/ui/find"
							/>
							<NavItem
								iconClasses="fa fa-twitter"
								text="Tweet to Air"
								link="http://cnnitouch.turner.com/apps/twitter_snapper/ui/dist/#/"
							/>
							<NavItem
								iconClasses="fa fa-forward"
								text="StillStringer"
								link="/apps/stillstringer"
							/>
							<NavItem
								iconClasses="glyphicon glyphicon-equalizer"
								text="Biz Data"
								link="/utilities/markets_db/monitor.php"
							/>
							<NavToolsMenu />
						</ul>
						<ul className="nav navbar-nav navbar-right">
							<li>
								<a href="/contact.php">
									<span className="glyphicon glyphicon-earphone"></span>
									&nbsp;Contact John
								</a>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		);
	}
}
export default Nav;
