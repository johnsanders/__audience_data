import React from "react";

class NavToolsMenu extends React.Component {
	constructor(props) {
		super(props);
		this.state = {active:false};
		this.handleMouseEnter = this.handleMouseEnter.bind(this);
		this.handleMouseLeave = this.handleMouseLeave.bind(this);
		this.handleClick = this.handleClick.bind(this);
	}
	handleMouseEnter() {
		this.setState({active:true});
	}
	handleMouseLeave() {
		this.setState({active:false});
	}
	handleClick(e) {
		e.preventDefault();
	}
	navigate(e){
		window.location.href = e.target.getAttribute("href");
	}
	render() {
		return (
			<li className="dropdown" onMouseEnter={this.handleMouseEnter} onMouseLeave={this.handleMouseLeave} onClick={this.handleClick}>
				<a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
					<span className="glyphicon glyphicon-wrench"></span>
						&nbsp;Tools
					<span className="caret"></span>
				</a>
				<ul className="dropdown-menu" style={{display:this.state.active ? "inherit" : "none"}}>
					<li>
						<a href="/apps/twitter_snapper/ui/dist/#/renderStatus" onClick={this.navigate}>
							Render Status
						</a>
					</li>
					<li role="separator" className="divider"></li>
					<li>
						<a href="/transcode/ftp.php" onClick={this.navigate}>
							Videodrop Importer
						</a>
					</li>
					<li>
						<a href="/transcode/ms2import-status.php" onClick={this.navigate}>
							Video Import Status
						</a>
					</li>
					<li>
						<a href="/transcode/allvideos.php" onClick={this.navigate}>
							View/Manage Videos
						</a>
					</li>
					<li role="separator" className="divider"></li>
					<li>
						<a href="/utilities/newsimages.php" onClick={this.navigate}>
							Newsimages Emailer
						</a>
					</li>
					<li role="separator" className="divider"></li>
					<li>
						<a href="/apps/misc/usmarketscountdown/usmarketscountdown.html" onClick={this.navigate}>
							Market Countdown
						</a>
					</li>
					<li>
						<a href="/apps/misc/usmarketscountdown/usmarketscountdown_espanol.html" onClick={this.navigate}>
							Market Countdown Esp.
						</a>
					</li>
					<li>
						<a href="/apps/logoloops/NSLogoLoop.html" onClick={this.navigate}>
							News Stream Logo
						</a>
					</li>
					<li>
						<a href="/apps/logoloops/NSBGLoop.html" onClick={this.navigate}>
							News Stream Bkg
						</a>
					</li>
				</ul>
			</li>
		);
	}
}
export default NavToolsMenu;
