import React from "react";
import {Route} from "react-router-dom"
import PropTypes from "prop-types";

const LiNavLink = (props) => {
	const {to, exact, strict, activeClassName, className, isActive: getIsActive, ...rest} = props;
	return (
		<Route
			path={typeof to === "object" ? to.pathname : to}
			exact={exact}
			strict={strict}
		>
			{({location, match}) => {
				const isActive = !!(getIsActive ? getIsActive(match, location) : match);
				return (
					<li
						className={isActive ? [activeClassName, className].join(" ") : className}
					>
						<a
							href={to}
							{...rest}
						/>
					</li>
				);
			}}
		</Route>
	);
};
LiNavLink.propTypes = {
	exact:PropTypes.bool,
	strict:PropTypes.bool,
	activeClassName:PropTypes.string,
	className:PropTypes.string,
	isActive:PropTypes.bool,
	getIsActive:PropTypes.func
};
export default LiNavLink;
