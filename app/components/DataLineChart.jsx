import React from "react";
import autobind from "react-autobind";
import {LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip, Legend} from  "recharts";
import {CustomTooltipRange, CustomTooltipSingle} from "./CustomTooltip";
import commaNumber from "comma-number";
import dataLabels from "../config/dataLabels"
import colors from "../config/colors";
import moment from "moment";

const DataLineChart = props => (
	<div style={{display:"flex", flexFlow:"row wrap", justifyContent:"center", position:"relative", left:"-150px"}}>
		{
			props.topics ? 
				<div className="text-center" style={{flex:"1 100%", marginBottom:"20px", position:"relative", left:"150px" }}>
					<h4>{moment(props.uploadedTime).format("D MMMM, YYYY")}</h4>	
					<h5>{"Topics: " + props.topics}</h5>
				</div>
			: null
		}
		<LineChart
			width={1000}
			height={500}
			data={props.chartData}
			margin={{top:5, right:50, bottom:5, left:0}}
		>
			<CartesianGrid stroke="#CCC" strokeDasharray="5 5" />
			<XAxis
				dataKey={props.formatXAxis}
				minTickGap={30}
			/>
			<YAxis tickFormatter={props.formatYAxis}/>
			<Legend 
				align="left"
				width={200}
				layout="vertical"
				iconSize={20}
				wrapperStyle={{top:"40px", right:-170, padding:"5px", backgroundColor:'#f5f5f5', border:'1px solid #d5d5d5', borderRadius:3, lineHeight:'40px'}} />
			<Tooltip
				content={
					props.selectedCategory.includes("retention")
						? <CustomTooltipSingle items={props.selectedItems}/>
						: <CustomTooltipRange />
					}
			/>
			{
				props.selectedItems.map( (item, i) => {
				const displayName = props.getLabel(item, props.selectedCategory);
					return (
						<Line
							key={"line_" + item}
							name={displayName}
							type="monotone"
							dataKey={item}
							stroke={colors[i]}
							strokeWidth={2}
							connectNulls={true}
						/>
					);
				})
			}
		</LineChart>
	</div>
);
export default DataLineChart;
