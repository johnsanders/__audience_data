import React from "react";
import autobind from "react-autobind";
import {BarChart, Bar, CartesianGrid, XAxis, YAxis, Tooltip, Legend} from  "recharts";
import {CustomTooltipTopics} from "./CustomTooltip";
import commaNumber from "comma-number";
import dataLabels from "../config/dataLabels"
import colors from "../config/colors";

class DataBarChart extends React.Component {
	constructor(props) {
		super(props);
		this.barsPerPage = 5;
		this.state = {page:0};
		this.buttonStyle = {
			fontSize:"20px",
			marginRight:"10px"
		}
		autobind(this);
	};
	onPageChange(e){
		const newPage = e.target.id === "prevPage" ? this.state.page - 1 : this.state.page + 1;
		if (newPage < 0 || newPage > this.props.chartData.length - this.barsPerPage) {
			return;
		} 
		this.setState({page:newPage});
	}
	render(){
		const chartData = this.props.chartData.slice(this.state.page, this.state.page + this.barsPerPage);
		return (
			<div className="text-center" style={{display:"flex", justifyContent:"center", flexFlow:"row wrap"}}>
				<div style={{flex:"1 100%", marginBottom:"20px"}}>
					<span
						id="prevPage"
						style={this.buttonStyle}
						className={(this.state.page===0 ? "disabled " : "") + "btn btn-default glyphicon glyphicon-arrow-left"}
						onClick={this.onPageChange}
					/>
					<span
						id="nextPage"
						style={this.buttonStyle}
						className={(this.state.page >= this.props.chartData.length - this.barsPerPage ? "disabled " : "") + "btn btn-default glyphicon glyphicon-arrow-right"}
						onClick={this.onPageChange}
					/>
				</div>
				<BarChart
					width={1000}
					height={500}
					data={chartData}
					margin={{top:5, right:0, bottom:5, left:0}}
				>
					<CartesianGrid stroke="#CCC" strokeDasharray="5 5" />
					<XAxis
						dataKey={this.props.formatXAxis}
						minTickGap={10}
					/>
					<YAxis 
						tickFormatter={this.props.formatYAxis}
						domain={[0, this.props.chartData[0][this.props.selectedItems[0]]]}
					/>
					<Tooltip
						content={CustomTooltipTopics}
					/>
					{
						this.props.selectedItems.map( (item, i) => {
						const displayName = this.props.getLabel(item, this.props.selectedCategory);
							return (
								<Bar
									key={"bar_" + item}
									name={displayName}
									dataKey={item}
									fill="#A0A0A0"
								/>
							);
						})
					}
				</BarChart>
			</div>
		);
	}
};
export default DataBarChart;
