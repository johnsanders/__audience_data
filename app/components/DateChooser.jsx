import React from "react";
import DatePicker from "react-datepicker";
import moment from "moment";
import 'react-datepicker/dist/react-datepicker.css';

const labelStyle = {
	fontSize:"20px",
	textWeight:"bold",
	display:"block",
	textAlign:"center"
};
const pickerStyle = {
	margin:"0 5px"
};

const DateChooser = props => (
	<div className="col-xs-6" style={{display:"flex"}}>
		<div style={pickerStyle}>
			<label
				htmlFor="showDate"
				style={labelStyle}
			>
				Show Date
			</label>
			<DatePicker
				id="showDate"
				className="form-control text-center"
				dayClassName={()=>"showDate"}
				dateFormat="D MMMM, YYYY"
				filterDate={date => (props.allShowDates.find(showDate => showDate.isSame(date, "d")))}
				selected={props.showDate}
				onChange={props.onDateChange}
				maxDate={moment()}
				minDate={moment("2017-05-05")}
			/>
		</div>
	</div>
);
export default DateChooser;
