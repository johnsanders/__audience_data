import React from "react";
import Spinner from "react-spinkit";
import PropTypes from "prop-types";
import Nav from "./nav/Nav";
import CategorySelector from "./CategorySelector";
import ItemsSelector from "./ItemsSelector";
import DataChartContainer from "./DataChartContainer";
import DateRangeChooser from "./DateRangeChooser";
import DateChooser from "./DateChooser";

const Home = props => (
	<div className="form-horizontal">
		<Nav />
		<div className="page-header" style={{marginTop:"80px"}}>
			<h1 className="text-center text-uppercase">AUDIENCE DATA</h1>
			<h2 className="text-center">CNN Talk Facebook Live</h2>
		</div>
		<div className="row form-inline">
			<CategorySelector 
				onCategoryChange={props.onCategoryChange}
			/>
			{
				props.selectedCategory.includes("retention") ?
					<DateChooser 
						showDate={props.showDate}
						allShowDates={props.allShowDates}
						onDateChange={props.onDateChange}
					/>
					:
					<DateRangeChooser 
						startDate={props.startDate}
						endDate={props.endDate}
						onDateChange={props.onDateChange}
					/>
			}
		</div>
		<ItemsSelector
			countries={props.countries}
			regions={props.regions}
			selectedItems={props.selectedItems}
			selectedCategory={props.selectedCategory}
			onSelectedItemChange={props.onSelectedItemChange}
		/>
		<DataChartContainer
			chartData={props.chartData}
			selectedCategory={props.selectedCategory}
			selectedItems={props.selectedItems}
		/>
	</div>
);
Home.propTypes = {
};
export default Home;
