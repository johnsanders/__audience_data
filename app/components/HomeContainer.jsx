import React from "react";
import autobind from "react-autobind";
import axios from "axios";
import moment from "moment";
import Home from "./Home";
import categories from "../config/categories";
import queryString from "query-string";
import {fetchFbData, fetchFbRegionData, calculateTopics} from "../helpers/dataHelpers";

class HomeContainer extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			chartData:[],
			regions:[],
			countries:[],
			selectedCategory:"Views",
			startDate:moment("2017-05-05"),
			endDate:moment()
		};
		this.firstLoad = true;
		this.state.selectedItems = categories.find( category => category.name === this.state.selectedCategory).defaultItems;
		autobind(this);
	}
	componentDidMount(){
		this.getData()
			.then( () => {
				if (this.firstLoad) {
					this.firstLoad = false;
					this.setShowDates(this.state.chartData);
				}
			});
		this.getRegions();
	}
	getRegions(){
		const url = "/apps/flashSQLinterface/getFbRegions.php";
		return axios.get(url)
			.then( data => {
				const regions = data.data;
				const countries = regions.reduce( (acc, region) => {
					const country = region.split("$")[1];
					acc.includes(country) ? null : acc.push(country);
					return acc;
				}, [] );
				this.setState({regions:data.data, countries:countries}) ;
			});
	}
	getData(){
		if (this.state.selectedCategory.includes("Geography")) {
			return fetchFbRegionData(this.state.selectedCategory, this.state.selectedItems, this.state.startDate, this.state.endDate)
				.then( data => this.setState({chartData:data}) );
		} else {
			return fetchFbData(this.state.selectedCategory, this.state.selectedItems, this.state.startDate, this.state.endDate, this.state.showDate)
				.then( data => {
					if ( this.state.selectedCategory.includes("topic") ) {
						this.setState({chartData:calculateTopics(data.data, this.state.selectedItems[0])});
					} else {
						this.setState({chartData:data.data});
					}
				})
		}
	}
	setShowDates(chartData){
		this.setState({
			allShowDates:chartData.map(datum => moment(datum.uploadedTime)),
			showDate:moment(chartData[chartData.length-1].uploadedTime)
		});
	}
	onCategoryChange(e){
		const defaultItems = categories.find( category => category.name === e.target.value ).defaultItems;
		this.setState({selectedCategory:e.target.value, selectedItems:defaultItems}, this.getData);
	}
	onDateChange(date, e){
		let key;
		if (e.target.classList.contains("startDate")){
			key = "startDate";
		} else if (e.target.classList.contains("endDate")) {
			key = "endDate";
		} else if (e.target.classList.contains("showDate")){
			key = "showDate"
		} else {
			return;
		}
		if ( (key === "startDate" && date.isAfter(this.state.endDate)) ||
			(key === "endDate" && date.isBefore(this.state.startDate))) {
			return;	
		}
		this.setState({[key]:date}, this.getData);
	}
	onSelectedItemChange(e){
		let newSelectedItems;
		if (this.state.selectedItems.includes(e.target.value)) {
			if (this.state.selectedItems.length === 1) {
				return;
			}
			newSelectedItems = this.state.selectedItems.filter( item => item !== e.target.value );
		} else {
			if (this.state.selectedCategory.includes("topic")) {
				newSelectedItems = [e.target.value];
			} else {
				newSelectedItems = this.state.selectedItems.concat([e.target.value]);
			}
		}
		this.setState({selectedItems:newSelectedItems}, this.getData);
	}
	render(){
		return (
			<Home 
				chartData={this.state.chartData}
				selectedCategory={this.state.selectedCategory}
				selectedItems={this.state.selectedItems}
				onCategoryChange={this.onCategoryChange}
				onSelectedItemChange={this.onSelectedItemChange}
				startDate={this.state.startDate}
				endDate={this.state.endDate}
				showDate={this.state.showDate}
				onDateChange={this.onDateChange}
				allShowDates={this.state.allShowDates}
				regions={this.state.regions}
				countries={this.state.countries}
			/>
		);
	}
}

export default HomeContainer;
