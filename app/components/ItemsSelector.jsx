import React from "react";
import dataLabels from "../config/dataLabels";
import categories from "../config/categories";

const getLabel = (item, selectedCategory) => (
	selectedCategory.includes("Geography") ? item.replace("$", ", ") : 
		dataLabels.find( label => label.key === item ).name
);

const ItemsSelector = props => {
	let items;
	switch (props.selectedCategory) {
		case "Geography (Countries)":
			items = props.countries;
			break;
		case "Geography (Regions)":
			items = props.regions;
			break;
		default:
			items = categories.find( cat => cat.name === props.selectedCategory ).items;
			break;
	}
	return (
		<div
			style={{
			display:"flex",
			flexWrap:"wrap",
			justifyContent:"center",
			marginBottom:"20px",
			paddingBottom:"20px",
			borderBottom: "1px solid #eee"
			}}
		>
			{
				items.map( item => {
					return (
						<div 
							className="checkbox"
							style={{
								display:"inline-flex",
								margin:"10px",
								width:"200px"

							}}
							key={"item_" + item}
						>
							<label>
								<input
									type="checkbox"
									value={item}
									checked={props.selectedItems.includes(item)}
									onChange={props.onSelectedItemChange}
								/>
								{getLabel(item, props.selectedCategory)}
							</label>
						</div>
					)	
				})
			}
		</div>
	)
};

export default ItemsSelector;
