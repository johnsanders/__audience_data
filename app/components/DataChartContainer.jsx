import React from "react";
import autobind from "react-autobind";
import moment from "moment";
import commaNumber from "comma-number";
import dataLabels from "../config/dataLabels"
import colors from "../config/colors";
import leftPad from "left-pad";
import categories from "../config/categories";
import DataLineChart from "./DataLineChart";
import DataBarChart from "./DataBarChart";

class DataChartContainer extends React.Component {
	constructor(props){
		super(props);
		autobind(this);
	}
	getCategory(){
		return categories.find( category => this.props.selectedCategory === category.name );
	}
	formatXAxis(data) {
		const xAxis = this.getCategory().xAxis;
		if (xAxis === "uploadedTime") {
			return moment(data.uploadedTime).format("D MMM.");
		} else if (xAxis === "time"){
			const duration = moment.duration(parseInt(data.time), "seconds");
			return duration.hours().toString() + ":" + leftPad(duration.minutes().toString(), 2, "0");
		} else {
			return data[xAxis];
		}
	}
	formatYAxis(val) {
		return val > 1 || val === 0 ? commaNumber(val) : (val * 100).toFixed(1) + "%";
	};
	getChartData(){
		const dataRoot = this.getCategory().dataRoot;
		if (dataRoot === "root") {
			return this.props.chartData;
		} else {
			return [this.props.chartData[0][dataRoot]][0];
		}
	}
	getLabel(item, selectedCategory) {
		return selectedCategory.includes("Geography") ? item.replace("$", ", ") : 
			dataLabels.find( label => label.key === item ).name;
	};
	render(){
		return (
			this.props.selectedCategory.includes("topic") ? 
				<DataBarChart
					chartData={this.getChartData()}
					selectedItems={this.props.selectedItems}
					selectedCategory={this.props.selectedCategory}
					getDateLabel={this.getDateLabel}
					getLabel={this.getLabel}
					formatYAxis={this.formatYAxis}
					formatXAxis={this.formatXAxis}
				/>
			: 
				<DataLineChart
					chartData={this.getChartData()}
					selectedItems={this.props.selectedItems}
					selectedCategory={this.props.selectedCategory}
					getDateLabel={this.getDateLabel}
					getLabel={this.getLabel}
					formatYAxis={this.formatYAxis}
					formatXAxis={this.formatXAxis}
					topics={this.props.selectedCategory.includes("retention") ? this.props.chartData[0].topics : null}
					uploadedTime={this.props.selectedCategory.includes("retention") ? this.props.chartData[0].uploadedTime : null}
				/>
		);
	}
}
export default DataChartContainer;
