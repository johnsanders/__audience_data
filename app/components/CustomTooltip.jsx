import React from "react";
import commaNumber from "comma-number";
import moment from "moment";
import dataLabels from "../config/dataLabels";

const tooltipStyle = {
	border:"solid gray 1px",
	borderRadius:"3px",
	backgroundColor:"white",
	padding:"10px"
};
const formatNumber = val => (
	val > 1 ? commaNumber(val) : (val * 100).toFixed(1) + "%"
);
const formatDate = timestamp => (
	moment(timestamp).format("D MMMM, YYYY")
);
const CustomTooltipRange = props => (
	props.active ? (
		<div className="custom-tooltip" style={tooltipStyle}>
			<div style={{fontSize:"20px"}}>
				{formatDate(props.payload[0].payload.uploadedTime)}
			</div>
			<div>
				<strong>
					{props.payload[0].payload.topics.includes(",") ? "Topics: " : "Topic: "}
				</strong> 
				{props.payload[0].payload.topics}
			</div>
			{
				props.payload.map( item => {
					const labelInfo = dataLabels.find( point => point.key === item.dataKey );
					const label = labelInfo ? labelInfo.name : item.dataKey.replace("$", ", ");
					return (
						<div key={item.dataKey} className="dataLabel">
							<strong>{label}:</strong> {formatNumber(item.value)}
						</div>
					);
				})
			}
		</div>
	) : null
);
const CustomTooltipSingle = props => (
	props.active && props.payload ?  (
		<div className="custom-tooltip" style={tooltipStyle}>
			{
				props.items.map( (item, i) => (
					<div key={"tooltip_" + item}>
						{props.payload[i].name + ": " + formatNumber(props.payload[0].payload[item])}
					</div>
				))
			}	
		</div>
	) : null
);
const CustomTooltipTopics = props => (
	props.active && props.payload ?  (
		<div className="custom-tooltip" style={tooltipStyle}>
			<div style={{fontSize:"20px"}}>
				{props.label}
			</div>
			<div>
				{"Average " + props.payload[0].name + ": "}
				{commaNumber(props.payload[0].value)}
			</div>
		</div>
	) : null
);
export {CustomTooltipRange, CustomTooltipSingle, CustomTooltipTopics};
