<?php
	$dbPrefix = "";
	$where = "1";
	$table = '';

	if (isSet($_GET['where'])) {
		$where = $_GET['where'];
	};

	if (isSet($_GET['table'])) {
		$table = $_GET['table'];
	};

	if (isSet($_GET['orderby'])){
		$orderBy = $_GET['orderby'];
	}
	
	if (isSet($_GET['order'])){
		$order = $_GET['order'];
	} else {
		$order = "ASC";
	}

	if (isSet($_GET['limit'])){
		$limit = $_GET['limit'];
	}
	if (isSet($_GET['fields'])){
		$fields = $_GET['fields'];
	} else {
		$fields = "*";
	}

	$types = [
		"id" => "int",
		"videoID" => "string",
		"retrievedTime" => "date",
		"uploadedTime" => "date",
		"permalinkUrl" => "string",
		"description" => "string",
		"topics" => "string",
		"duration" => "int",
		"totalVideoViews" => "int",
		"viewsAutoplayed" => "int",
		"viewsClickedToPlay" => "int",
		"viewsSoundOn" => "int",
		"viewsSoundOff" => "int",
		"views30s" => "int",
		"views30sAutoplayed" => "int",
		"views30sClickedToPlay" => "int",
		"views10s" => "int",
		"views10sAutoplayed" => "int",
		"views10sClickedToPlay" => "int",
		"views10sSoundOn" => "int",
		"views10sSoundOff" => "int",
		"viewsRetentionGraphs" => "json",
		"reach" => "int",
		"engagementTotalLike" => "int",
		"engagementTotalShare" => "int",
		"engagementTotalComment" => "int",
		"engagementTotalCombined" => "int",
		"avgTimeWatched" => "float",
		"minutesViewedAgeGender_13_17_male" => "float",
		"minutesViewedAgeGender_13_17_female" => "float",
		"minutesViewedAgeGender_18_24_male" => "float",
		"minutesViewedAgeGender_18_24_female" => "float",
		"minutesViewedAgeGender_25_34_male" => "float",
		"minutesViewedAgeGender_25_34_female" => "float",
		"minutesViewedAgeGender_25_34_male" => "float",
		"minutesViewedAgeGender_25_34_female" => "float",
		"minutesViewedAgeGender_35_44_male" => "float",
		"minutesViewedAgeGender_35_44_female" => "float",
		"minutesViewedAgeGender_45_54_male" => "float",
		"minutesViewedAgeGender_45_54_female" => "float",
		"minutesViewedAgeGender_55_64_male" => "float",
		"minutesViewedAgeGender_55_64_female" => "float",
		"minutesViewedAgeGender_65_male" => "float",
		"minutesViewedAgeGender_65_female" => "float",
		"broadcastViewersRetention" => "json",
		"timeSpent" => "float",
		"viewsShared" => "int",
		"region" => "string",
		"country" => "string",
		"percent" => "float",
		"total_time" => "int"
	];

	$dsn = 'mysql:dbname=audience_data;host=127.0.0.1';
	$link = new mysqli('localhost', 'root', 'root', 'audience_data');
	mysqli_set_charset ($link, "utf8");
	$joinString = "r INNER JOIN talk_fb_insights i on r.video_id = i.videoID";

	$query = "SELECT $fields FROM $table";

	if ($table === "talk_fb_insights_regions" ) {
		$query .= " $joinString";
	}

	$query .= "	WHERE $where";

	if (isSet($orderBy)) {
		$query .= " ORDER BY $orderBy $order";
	}
	if (isSet($limit)) {
		$query .= " LIMIT $limit";
	}
	$result = $link->query($query)
	           or die("<b>error</b>: failed to execute query <i>$query</i>");

	$all = array();
	while($row = $result->fetch_assoc()){
		foreach ( $row as $key => $value ) {
			switch($key){
				case $types[$key] === "string":
					$row[$key] = strval($value);
					break;
				case $types[$key] === "int":
					$row[$key] = intval($value);
					break;
				case $types[$key] === "float":
					$row[$key] = floatval($value);
					break;
				case $types[$key] === "json":
					$row[$key] = json_decode($value);
					break;
			}
		}
		array_push($all, $row);
	}

	echo json_encode($all, JSON_PARTIAL_OUTPUT_ON_ERROR);

	function sortByOrder($a, $b) {
		return $a['id'] - $b['id'];
	}
	mysqli_close($link);
?>
