<?php

	function formatRegion($region, $country) {
		return ($region ? "$region$" : "") . $country;
	}

	$dsn = 'mysql:dbname=audience_data;host=127.0.0.1';
	$link = new mysqli('localhost', 'root', 'root', 'audience_data');

	$link->query("SET NAMES 'utf8'");
	$query = "SELECT region, country FROM talk_fb_insights_regions";

	$result = $link->query($query)
	           or die("<b>error</b>: failed to execute query <i>$query</i>");

	$all = array();




	while($row = $result->fetch_assoc()){
		$alreadyExists = array_filter($all, function($item){
			global $row;
			return formatRegion( $row["region"], $row["country"] ) === $item;
		});
		if (!$alreadyExists) {
			array_push($all, formatRegion($row["region"], $row["country"]));
		}
	};

	echo json_encode($all, JSON_PARTIAL_OUTPUT_ON_ERROR);

?>
